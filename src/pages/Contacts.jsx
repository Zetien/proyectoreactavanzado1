import { useRef, useContext } from "react";
import FormContact from "../components/Form-contact";
import { ThemeContext } from "../utils/theme.context";

export default function Contacts() {
  const ref = useRef(null);
  const context = useContext(ThemeContext);

  const printLogin = (values) => {
    console.log(values);
  };
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";
  return (
    <div className="container">
      <div className="d-grid" style={{ placeItems: "center" }}>
        <div className={"container-fluid bg-" + context.theme}>
          <FormContact onSubmit={printLogin} innerRef={ref} />
          <button
            className="btn btn-primary"
            onClick={() => {
              console.log("ok", ref.current);
              if (ref.current) {
                ref.current.submitForm();
              }
              context.toggle_theme("dark");
            }}
          >
            Enviar Comentario
          </button>
        </div>
      </div>
    </div>
  );
}
