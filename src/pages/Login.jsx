import { useContext, useRef } from "react";
import LoginForm from "../components/Login-form";
import { ThemeContext } from "../utils/theme.context";

const Login = () => {
  const ref = useRef(null);
  const context = useContext(ThemeContext);

  const login = (a) => {
    console.log(a);
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  return (
    <div className="w-100 h-100 d-flex align-items-center justify-content-center">
      <div className={"container-fluid bg-" + context.theme}>
        <div className="row">
          <div
            className="col-6"
            style={{
              backgroundSize: "cover",
              backgroundImage: "url(https://picsum.photos/800/300)",
            }}
          ></div>
          <div className="col-6 p-5">
            <h1 className={text_class}>ZETIEN CAR IV</h1>
            <LoginForm onSubmit={login} myRef={ref} />
            <button
              className="btn btn-primary w-100"
              disabled={ds}
              onClick={() => {
                console.log("ok", ref.current);
                if (ref.current) {
                  ref.current.submitForm();
                }
                context.toggle_theme("dark");
              }}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
