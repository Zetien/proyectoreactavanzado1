export function Information() {
  return (
    <div className="container">
      <div className="py-4">
        <h1 className="fs-1">Informacion</h1>
      </div>
      <div className="" style={{ textAlign: "justify" }}>
        <p>
          Lorem ipsum dolor sit amet consectetur adipiscing elit lobortis
          praesent, taciti congue aenean mauris sagittis curae aptent. Aliquet
          lectus hendrerit nam placerat per nostra, feugiat sed aliquam
          malesuada tempus, tempor neque orci mattis ligula. Leo euismod proin
          vehicula massa pretium pulvinar tempus arcu, interdum cras velit
          ligula dapibus sapien suscipit sociosqu, tristique habitant sed mattis
          semper et habitasse. Pretium odio tellus faucibus non aliquet aptent
          viverra nibh rutrum vehicula convallis hendrerit augue, parturient mi
          ad velit justo etiam litora id curabitur dui class urna. Mollis dis
          scelerisque aenean ut senectus a fringilla cras eros, nostra taciti
          mauris proin sagittis nulla varius dapibus sollicitudin, nam tincidunt
          dui nisi interdum gravida leo suspendisse. At cum turpis accumsan ac
          varius condimentum ut eros, pretium quis egestas ante hac sociosqu
          diam, duis non dictum nam erat maecenas purus. Facilisi nisi cursus
          habitasse orci aptent rutrum convallis sapien in pulvinar mauris cum
          iaculis sed ullamcorper dapibus ornare mattis, quam potenti molestie
          tempor bibendum blandit ligula elementum quis ultricies erat natoque
          nam sodales tellus ad lacus. Praesent accumsan faucibus fringilla
          posuere dis facilisis, sem magnis ante erat nam. Non magna nullam
          dignissim enim commodo conubia sapien integer libero, nec urna
          vulputate nisi erat fames hac sociis, eros lacus pellentesque nunc mi
          morbi ac tempor. Orci libero feugiat pretium velit tincidunt gravida
          parturient id himenaeos consequat nunc, aptent pulvinar tempus quisque
          inceptos ultricies cum congue penatibus nisi lectus sociosqu,
          tristique justo vulputate volutpat ridiculus lobortis fames senectus
          sem hendrerit. Felis lacinia urna tellus lectus habitasse viverra
          class accumsan taciti erat aenean, ullamcorper himenaeos fermentum
          ornare orci fringilla quisque semper ad.
        </p>

        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          atque recusandae harum perspiciatis sit vel voluptatum dicta dolores
          magni aperiam iste odit, quos modi provident, temporibus illum, neque
          facere quibusdam!
        </p>
      </div>
    </div>
  );
}
