import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";
import { Link } from "react-router-dom";

export function Navbar() {
  const context = useContext(ThemeContext);
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";
  let oppositeTheme = context.theme === "light" ? "dark" : "light";
  return (
    <div>
      <nav
        className={`navbar navbar-expand-lg  fluid navbar-${context.theme} bg-${context.theme}`}
      >
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <button
                className={`btn btn-outline-${oppositeTheme}`}
                onClick={() => {
                  context.toggle_theme();
                }}
                style={{ border: "none", outline: "none" }}
              >
                ZETIEN
              </button>
              <Link className="nav-link active" aria-current="page" to="/">
                Home
              </Link>
              <Link className="nav-link" to="/Login">
                Login
              </Link>
              <Link className="nav-link" to="/Contacts">
                Contacto
              </Link>
              <Link className="nav-link" to="/Information">
                Informacion
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}
