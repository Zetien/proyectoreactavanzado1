import { Formik, Form, Field } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";
export default function FormContact({ onSubmit, innerRef }) {
  const context = useContext(ThemeContext);
  const initialData = {
    name: "",
    email: "",
    address: "",
    phone: "",
    coment: "",
  };
  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  const text_class = context.theme === "dark" ? "text-white" : "";
  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={innerRef}>
      {() => {
        return (
          <div className="py-4">
            <h1 className="text-center">Formulario de Contacto</h1>
            <Form>
              <div className="form-group my-3">
                <label htmlFor="name">Nombre</label>
                <Field
                  className="form-control"
                  id="name"
                  type="text"
                  name="name"
                  placeholder="Ingrese su nombre"
                ></Field>
              </div>
              <div className="form-group my-3">
                <label htmlFor="email">Correo</label>
                <Field
                  className="form-control"
                  id="email"
                  type="email"
                  name="email"
                  placeholder="Ingrese su correo"
                ></Field>
              </div>
              <div className="form-group my-3">
                <label htmlFor="address">Direccion</label>
                <Field
                  className="form-control"
                  id="address"
                  type="text"
                  name="address"
                  placeholder="Ingrese su direccion"
                ></Field>
              </div>
              <div className="form-group my-3">
                <label htmlFor="phone">Numero Telefono</label>
                <Field
                  className="form-control"
                  id="phone"
                  type="number"
                  name="phone"
                  placeholder="Ingrese su numero de telefono o celular"
                ></Field>
              </div>
              <div className="form-group my-3">
                <label htmlFor="coment">Comentario</label>
                <Field
                  as="textarea"
                  className="form-control"
                  id="coment"
                  type="text"
                  name="coment"
                  placeholder="Ingrese su comentario"
                ></Field>
              </div>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
}
