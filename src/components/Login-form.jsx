import { Formik, Form, Field } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

export default function LoginForm({ onSubmit, innerRef }) {
  const context = useContext(ThemeContext);
  const initialData = {
    user_name: "",
    password: "",
  };
  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  const text_class = context.theme === "dark" ? "text-white" : "";
  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={innerRef}>
      {() => {
        return (
          <div className="py-4">
            <h1 className="text-center">Login de Usuario</h1>
            <Form>
              <div className="form-group my-3">
                <label htmlFor="username">Nombre de usuario</label>
                <Field
                  className="form-control"
                  id="username"
                  type="text"
                  name="user_name"
                  placeholder="Ingrese su nombre"
                ></Field>
              </div>
              <div className="form-group my-3">
                <label htmlFor="pass">Contraseña</label>
                <Field
                  className="form-control"
                  id="pass"
                  type="password"
                  name="password"
                  placeholder="Ingrese su contraseña"
                ></Field>
              </div>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
}
