import "./App.css";
import Login from "./pages/Login";
import { Navbar } from "./components/Navbar";
import { Route, Routes } from "react-router-dom";
import { Home } from "./pages/Home";
import Contacts from "./pages/Contacts";
import { Information } from "./pages/Information";
import ThemeProvider from "./utils/theme.context";
import { useState } from "react";

function App() {
  const [theme, setTheme] = useState("bg-light");
  return (
    <div className="App">
      <div>
        <ThemeProvider _theme="light">
          <Navbar />
          <Routes>
            {<Route path="/" element={<Home />} />}
            {<Route path="/Login" element={<Login />} />}
            {<Route path="/Contacts" element={<Contacts />} />}
            {<Route path="/Information" element={<Information />} />}
          </Routes>
        </ThemeProvider>
      </div>
    </div>
  );
}

export default App;
